#!/bin/bash -x
set -e
echo "Build started on $(date)"
echo "==============================================================="
echo "Build Pointzi demos"
security unlock-keychain -p $BUILD_PASSWORD "/Users/hawk/Library/Keychains/login.keychain-db"

pushd .
cd Example_DynamicFramework
pod update
fastlane beta HOCKEYAPP_TOKEN:$HOCKEYAPP_TOKEN
popd
